﻿#include "treemodel.h"
#include "item/localdatadiritem.h"
#include "item/serverlistitem.h"

#include <QtDebug>
#include <QSettings>

TreeModel::TreeModel(QObject *parent)
  : QStandardItemModel(parent)
{
  initialize();
}

void TreeModel::initialize()
{
  loadSettings();

  // ヘッダーラベルを設定する
  setHorizontalHeaderLabels(
        QStringList()
        << tr("Items")
        );

  // ローカルデータディレクトリをルートの下に置く
  invisibleRootItem()->appendRow(new LocalDataDirItem);

  // サーバリストをルートの下に置く
  invisibleRootItem()->appendRow(new ServerListItem);
}

void TreeModel::refreshChildren(const QModelIndex &index)
{
  // 無効なインデックスなら終了する
  if (!index.isValid())
    return;

  // ツリーアイテムを取得する
  TreeItem* item = (TreeItem*)itemFromIndex(index);
  Q_ASSERT(item);

  // 子アイテムをすべて削除する
  item->removeRows(0, item->rowCount());

  item->refreshChildren();
}

QVariant TreeModel::data(const QModelIndex &index, int role) const
{
  if (role == Qt::DisplayRole && index.isValid())
  {
    TreeItem* item = (TreeItem*)itemFromIndex(index);
    return item->displayData(*this);
  }
  return QStandardItemModel::data(index, role);
}

void TreeModel::loadSettings()
{
  QSettings settings;
  dbTextType_ = (DbTextType)settings.value(
        "DbTextType"
        , (int)DbTextType::Filename
        ).toInt();
  noteTextType_ = settings.value(
        "NoteTextType"
        , NoteTextType_NoteId
        ).toUInt();
}

void TreeModel::selectedItem(const QModelIndex &index)
{
  TreeItem* item = (TreeItem*)itemFromIndex(index);
  emit writeProperties(item->propertyMap());
}
