﻿#include "treeitem.h"

TreeItem::TreeItem(const QString& text, const QIcon& icon)
  : QStandardItem(icon, text)
{
  setEditable(false);
}

void TreeItem::insertProperty(const QString& key, const QVariant& value)
{
  propertyMap_.insert(key, value);
}

QVariant TreeItem::displayData(const TreeModel& /*model*/)
{
  return data(Qt::DisplayRole);
}
