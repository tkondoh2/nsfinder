﻿#include "mainwindow.h"
#include <QApplication>

#include <ntlx/status.h>

int main(int argc, char *argv[])
{
  // Notes APIを初期化
  if (ntlx::initEx(argc, argv).failure())
    return -1;

  // アプリケーション設定
  QApplication a(argc, argv);
  a.setApplicationName("nsfinder");
  a.setOrganizationName("Chiburu Systems");

  MainWindow w;
  w.show();
  int result = a.exec();

  // Notes APIを終了
  ntlx::term();

  return result;
}
