﻿#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "treemodel.h"
#include "optionsdialog.h"

MainWindow::MainWindow(QWidget *parent)
  : QMainWindow(parent)
  , ui_(new Ui::MainWindow)
{
  ui_->setupUi(this);

  // ツリーモデルを作成する
  model_ = new TreeModel(this);

  // ツリーモデルとツリービューを関連づける
  ui_->treeView->setModel(model_);

  // ツリービューでカスタムコンテキストメニューがリクエストされたら
  // treeContextMenuにスロットする
  connect(ui_->treeView, &TreeView::customContextMenuRequested
          , this, &MainWindow::treeContextMenu
          );

  // ツリービューがクリックされたら
  // モデルのアイテム選択にスロットする
  connect(ui_->treeView, &TreeView::clicked
          , model_, &TreeModel::selectedItem
          );

  // モデルがプロパティ出力したら
  // プロパティテーブルの出力にスロットする
  connect(model_, &TreeModel::writeProperties
          , ui_->propTable, &PropertiesTable::write
          );

  // オプションアクションをトリガーしたら
  // オプションダイアログの実行にスロットする
  connect(ui_->action_Options, &QAction::triggered
          , this, &MainWindow::execOptionsDialog
          );

  // 子アイテム更新アクションをトリガーしたら
  // ツリービューの子アイテム更新にスロットする
  connect(ui_->actionRefresh_Children, &QAction::triggered
          , ui_->treeView, &TreeView::refreshChildren
          );
}

MainWindow::~MainWindow()
{
  delete ui_;
}

void MainWindow::treeContextMenu(const QPoint &pos)
{
  // マウス位置にアイテムがなければ終了する
  QModelIndex index = ui_->treeView->indexAt(pos);
  if (!index.isValid()) return;

  // コンテキストメニューを開く
  QMenu menu;
  menu.addAction(ui_->actionRefresh_Children);
  menu.exec(QCursor::pos());
}

void MainWindow::execOptionsDialog()
{
  // オプションダイアログを実行する
  OptionsDialog dialog;
  dialog.exec();

  // 最新の設定情報をロードして、ビューを再描画する
  model_->loadSettings();
  ui_->treeView->repaint();
}
