﻿#include "treeview.h"

#include <QMenu>
#include <QAction>

#include "treemodel.h"

TreeView::TreeView(QWidget* parent)
  : QTreeView(parent)
{
  // コンテキストメニューポリシーをカスタムにする
  setContextMenuPolicy(Qt::CustomContextMenu);
}

void TreeView::refreshChildren()
{
  // 現在のインデックスを取得する
  QModelIndex index = currentIndex();

  // ツリーモデルの子アイテム更新メソッドを呼び出す
  treeModel()->refreshChildren(index);

  // 子インデックスを展開する
  expand(index);

  // 列幅をコンテンツに応じて調整する
  resizeColumnToContents(0);
}
