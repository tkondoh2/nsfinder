#-------------------------------------------------
#
# Project created by QtCreator 2017-03-06T16:55:45
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = nsfinder
TEMPLATE = app

VERSION = 0.9.4

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += main.cpp\
        mainwindow.cpp \
    treeview.cpp \
    treemodel.cpp \
    treeitem.cpp \
    propertiestable.cpp \
    optionsdialog.cpp \
    item/diritem.cpp \
    item/localdatadiritem.cpp \
    item/serveritem.cpp \
    item/notefileitem.cpp \
    item/noteitem.cpp \
    item/documentitem.cpp \
    item/serverlistitem.cpp \
    item/dbinfoitem.cpp \
    item/aclgroupitem.cpp \
    item/designelementsitem.cpp \
    item/documentgroupitem.cpp \
    item/profilegroupitem.cpp \
    item/deletionstubsitem.cpp \
    item/fileitem.cpp \
    item/itemitem.cpp \
    item/compositedataitem.cpp \
    item/designnoteitem.cpp \
    item/cditem.cpp \
    item/cd/cdotheritem.cpp \
    item/cd/cdtextitem.cpp \
    item/cd/cdfonttable.cpp

HEADERS  += mainwindow.h \
    treeview.h \
    treemodel.h \
    treeitem.h \
    propertiestable.h \
    optionsdialog.h \
    item/diritem.h \
    item/localdatadiritem.h \
    item/serveritem.h \
    item/notefileitem.h \
    item/noteitem.h \
    item/documentitem.h \
    definitions.h \
    item/serverlistitem.h \
    item/dbinfoitem.h \
    item/aclgroupitem.h \
    item/designelementsitem.h \
    item/documentgroupitem.h \
    item/profilegroupitem.h \
    item/deletionstubsitem.h \
    item/fileitem.h \
    item/itemitem.h \
    item/compositedataitem.h \
    item/designnoteitem.h \
    item/cditem.h \
    item/cd/cdotheritem.h \
    item/cd/cdtextitem.h \
    item/cd/cdfonttable.h

FORMS    += mainwindow.ui \
    optionsdialog.ui

DISTFILES += .gitignore

win32 {
    DEFINES += W32 NT
    LIBS += -lntlx0
}
else:macx {
    # MACのDWORDをunsigned int(4バイト)にする
    # DEFINES += MAC
    DEFINES += MAC LONGIS64BIT
    LIBS += -lntlx.0
}
else:unix {
    DEFINES += UNIX LINUX W32
    QMAKE_CXXFLAGS += -std=c++0x
    LIBS += -lntlx
}

LIBS += -lnotes

INCLUDEPATH += $$PWD/../ntlx
DEPENDPATH += $$PWD/../ntlx

RESOURCES += \
    nsfinder.qrc
