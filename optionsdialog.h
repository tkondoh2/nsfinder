﻿#ifndef OPTIONSDIALOG_H
#define OPTIONSDIALOG_H

#include <QDialog>

#include "definitions.h"

namespace Ui {
class OptionsDialog;
}

class QAbstractButton;

/**
 * @brief オプションダイアログ
 */
class OptionsDialog
    : public QDialog
{
  Q_OBJECT

public:
  /**
   * @brief コンストラクタ
   * @param parent 親ウィジェットへのポインタ
   */
  explicit OptionsDialog(QWidget *parent = 0);

  /**
   * @brief デストラクタ
   */
  ~OptionsDialog();

public slots:

  /**
   * @brief 設定情報をロードする
   */
  void loadSettings();

  /**
   * @brief データベーステキストを選択する
   * @param button 選択されたボタン
   */
  void selectDbText(QAbstractButton* button);

  /**
   * @brief ノートテキストを選択する
   */
  void selectNoteText(bool);

private:
  Ui::OptionsDialog *ui_;
};

#endif // OPTIONSDIALOG_H
