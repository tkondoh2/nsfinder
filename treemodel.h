﻿#ifndef TREEMODEL_H
#define TREEMODEL_H

#include <QStandardItemModel>

#include "definitions.h"

/**
 * @brief ツリーモデルクラス
 */
class TreeModel : public QStandardItemModel
{
  Q_OBJECT
public:
  /**
   * @brief コンストラクタ
   * @param parent 親ウィジェットへのポインタ
   */
  explicit TreeModel(QObject *parent = 0);

  /**
   * @brief 初期設定をする
   */
  void initialize();

  /**
   * @brief 子アイテムを更新する
   * @param index 更新元となる親アイテムへのインデックス
   */
  void refreshChildren(const QModelIndex& index);

  /**
   * @brief データベーステキストタイプを取得する
   * @return データベーステキストタイプ
   */
  DbTextType dbTextType() const { return dbTextType_; }

  /**
   * @brief ノートテキストタイプを取得する
   * @return ノートテキストタイプ
   */
  uint noteTextType() const { return noteTextType_; }

  /**
   * @brief インデックス位置のアイテムのロール別データを取得する
   * @param index 対象インデックス
   * @param role ロール
   * @return アイテムデータ
   */
  virtual QVariant data(const QModelIndex &index, int role) const;

signals:

  /**
   * @brief プロパティ書き込みシグナル
   * @param map プロパティマップ
   */
  void writeProperties(const PropertyMap& map);

public slots:

  /**
   * @brief 設定情報をロードする
   */
  void loadSettings();

  /**
   * @brief インデックス位置のアイテム選択時の動作をする
   * @param index インデックス
   */
  void selectedItem(const QModelIndex& index);

private:
  DbTextType dbTextType_;
  uint noteTextType_;
};

#endif // TREEMODEL_H
