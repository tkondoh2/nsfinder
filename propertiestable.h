﻿#ifndef PROPERTIESTABLE_H
#define PROPERTIESTABLE_H

#include <QTableView>
#include <QStandardItemModel>
#include "definitions.h"

/**
 * @brief プロパティテーブルクラス
 */
class PropertiesTable
    : public QTableView
{
  Q_OBJECT
public:
  /**
   * @brief コンストラクタ
   * @param parent 親ウィジェットへのポインタ
   */
  explicit PropertiesTable(QWidget *parent = 0);

public slots:

  /**
   * @brief プロパティマップを表示する
   * @param map プロパティ情報
   */
  void write(const PropertyMap& map);

private:
  QStandardItemModel model_;
};

#endif // PROPERTIESTABLE_H
