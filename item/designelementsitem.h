﻿#ifndef DESIGNELEMENTSITEM_H
#define DESIGNELEMENTSITEM_H

#include "fileitem.h"
#include <ntlx/status.h>

class DesignElementsItem
    : public FileItem
{
public:
  DesignElementsItem(
      WORD noteClass
      , const QString& text
      , const QIcon& icon = QIcon(":/icons/settings.png")
      );

  virtual void refreshChildren();

protected:
  WORD noteClass_;
  QString titleText_;
};

#endif // DESIGNELEMENTSITEM_H
