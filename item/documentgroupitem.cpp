﻿#include "item/documentgroupitem.h"
#include "item/documentitem.h"
#include "item/notefileitem.h"

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <stdnames.h>

#if defined(NT)
#pragma pack(pop)
#endif

DocumentGroupItem::DocumentGroupItem(
    const QString& text
    , const QIcon& icon
    )
  : FileItem(text, icon)
{
}

void DocumentGroupItem::refreshChildren()
{
  ntlx::Database db(ntlx::PathSet(path_, server_, port_));

  ntlx::SummaryList list;
  if (db.collectNotes(list, NOTE_CLASS_DOCUMENT).failure())
    return;

  for (auto it = list.begin(); it != list.end(); ++it)
  {
    DocumentItem* item = refreshNote<DocumentItem>(*it);
    item->computeWindowTitle(db);
  }

  setData(QObject::tr("Documents (%1)").arg(rowCount()), Qt::DisplayRole);
}
