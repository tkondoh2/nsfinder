﻿#include "fileitem.h"

FileItem::FileItem(
    const QString& text
    , const QIcon& icon
    )
  : TreeItem(text, icon)
  , path_()
  , server_()
  , port_()
{
}

void FileItem::assign(const FileItem &other)
{
  server_ = other.server_;
  port_ = other.port_;
  path_ = other.path_;
}
