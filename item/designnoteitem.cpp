﻿#include "designnoteitem.h"
#include <ntlx/note.h>
#include <ntlx/item/text.h>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <stdnames.h>

#if defined(NT)
#pragma pack(pop)
#endif

DesignNoteItem::DesignNoteItem(GLOBALINSTANCEID giid)
  : NoteItem(giid, QIcon())
  , title_()
{
}

QString DesignNoteItem::getTitle() const
{
  return title_;
}

void DesignNoteItem::getDesignTitle(ntlx::Note& note)
{
  title_ = note.getItem<ntlx::item::Text>(FIELD_TITLE).value();
}

void DesignNoteItem::getDesignFlags(ntlx::Note& note)
{
  QString flags = note.getItem<ntlx::item::Text>(DESIGN_FLAGS).value();
  QStringList list;
  if (flags.indexOf(DESIGN_FLAG_ADD) >= 0) list.append("ADD");
  if (flags.indexOf(DESIGN_FLAG_ANTIFOLDER) >= 0) list.append("ANTIFOLDER");
  if (flags.indexOf(DESIGN_FLAG_BACKGROUND_FILTER) >= 0) list.append("BACKGROUND_FILTER");
  if (flags.indexOf(DESIGN_FLAG_INITBYDESIGNONLY) >= 0) list.append("INITBYDESIGNONLY");
  if (flags.indexOf(DESIGN_FLAG_NO_COMPOSE) >= 0) list.append("NO_COMPOSE");
  if (flags.indexOf(DESIGN_FLAG_CALENDAR_VIEW) >= 0) list.append("CALENDAR_VIEW");
  if (flags.indexOf(DESIGN_FLAG_NO_QUERY) >= 0) list.append("NO_QUERY");
  if (flags.indexOf(DESIGN_FLAG_DEFAULT_DESIGN) >= 0) list.append("DEFAULT_DESIGN");
  if (flags.indexOf(DESIGN_FLAG_MAIL_FILTER) >= 0) list.append("MAIL_FILTER");
  if (flags.indexOf(DESIGN_FLAG_PUBLICANTIFOLDER) >= 0) list.append("PUBLICANTIFOLDER");
  if (flags.indexOf(DESIGN_FLAG_FOLDER_VIEW) >= 0) list.append("FOLDER_VIEW");
  if (flags.indexOf(DESIGN_FLAG_V4AGENT) >= 0) list.append("V4AGENT");
  if (flags.indexOf(DESIGN_FLAG_VIEWMAP) >= 0) list.append("VIEWMAP");
  if (flags.indexOf(DESIGN_FLAG_FILE) >= 0) list.append("FILE");
  if (flags.indexOf(DESIGN_FLAG_OTHER_DLG) >= 0) list.append("OTHER_DLG");
  if (flags.indexOf(DESIGN_FLAG_JAVASCRIPT_LIBRARY) >= 0) list.append("JAVASCRIPT_LIBRARY");
  if (flags.indexOf(DESIGN_FLAG_V4PASTE_AGENT) >= 0) list.append("V4PASTE_AGENT");
  if (flags.indexOf(DESIGN_FLAG_IMAGE_RESOURCE) >= 0) list.append("IMAGE_RESOURCE");
  if (flags.indexOf(DESIGN_FLAG_JAVA_AGENT) >= 0) list.append("JAVA_AGENT");
  if (flags.indexOf(DESIGN_FLAG_JAVA_AGENT_WITH_SOURCE) >= 0) list.append("JAVA_AGENT_WITH_SOURCE");
  if (flags.indexOf(DESIGN_FLAG_MOBILE_DIGEST) >= 0) list.append("MOBILE_DIGEST");
  if (flags.indexOf(DESIGN_FLAG_XSPPAGE) >= 0) list.append("XSPPAGE");
  if (flags.indexOf(DESIGN_FLAG_CONNECTION_RESOURCE) >= 0) list.append("CONNECTION_RESOURCE");
  if (flags.indexOf(DESIGN_FLAG_LOTUSSCRIPT_AGENT) >= 0) list.append("LOTUSSCRIPT_AGENT");
  if (flags.indexOf(DESIGN_FLAG_DELETED_DOCS) >= 0) list.append("DELETED_DOCS");
  if (flags.indexOf(DESIGN_FLAG_QUERY_MACRO_FILTER) >= 0) list.append("QUERY_MACRO_FILTER");
  if (flags.indexOf(DESIGN_FLAG_SITEMAP) >= 0) list.append("SITEMAP");
  if (flags.indexOf(DESIGN_FLAG_NEW) >= 0) list.append("NEW");
  if (flags.indexOf(DESIGN_FLAG_HIDE_FROM_NOTES) >= 0) list.append("HIDE_FROM_NOTES");
  if (flags.indexOf(DESIGN_FLAG_QUERY_V4_OBJECT) >= 0) list.append("QUERY_V4_OBJECT");
  if (flags.indexOf(DESIGN_FLAG_PRIVATE_STOREDESK) >= 0) list.append("PRIVATE_STOREDESK");
  if (flags.indexOf(DESIGN_FLAG_PRESERVE) >= 0) list.append("PRESERVE");
  if (flags.indexOf(DESIGN_FLAG_PRIVATE_1STUSE) >= 0) list.append("PRIVATE_1STUSE");
  if (flags.indexOf(DESIGN_FLAG_QUERY_FILTER) >= 0) list.append("QUERY_FILTER");
  if (flags.indexOf(DESIGN_FLAG_AGENT_SHOWINSEARCH) >= 0) list.append("AGENT_SHOWINSEARCH");
  if (flags.indexOf(DESIGN_FLAG_REPLACE_SPECIAL) >= 0) list.append("REPLACE_SPECIAL");
  if (flags.indexOf(DESIGN_FLAG_PROPAGATE_NOCHANGE) >= 0) list.append("PROPAGATE_NOCHANGE");
  if (flags.indexOf(DESIGN_FLAG_V4BACKGROUND_MACRO) >= 0) list.append("V4BACKGROUND_MACRO");
  if (flags.indexOf(DESIGN_FLAG_SCRIPTLIB) >= 0) list.append("SCRIPTLIB");
  if (flags.indexOf(DESIGN_FLAG_VIEW_CATEGORIZED) >= 0) list.append("VIEW_CATEGORIZED");
  if (flags.indexOf(DESIGN_FLAG_DATABASESCRIPT) >= 0) list.append("DATABASESCRIPT");
  if (flags.indexOf(DESIGN_FLAG_SUBFORM) >= 0) list.append("SUBFORM");
  if (flags.indexOf(DESIGN_FLAG_AGENT_RUNASWEBUSER) >= 0) list.append("AGENT_RUNASWEBUSER");
  if (flags.indexOf(DESIGN_FLAG_AGENT_RUNASINVOKER) >= 0) list.append("AGENT_RUNASINVOKER");
  if (flags.indexOf(DESIGN_FLAG_PRIVATE_IN_DB) >= 0) list.append("PRIVATE_IN_DB");
  if (flags.indexOf(DESIGN_FLAG_IMAGE_WELL) >= 0) list.append("IMAGE_WELL");
  if (flags.indexOf(DESIGN_FLAG_WEBPAGE) >= 0) list.append("WEBPAGE");
  if (flags.indexOf(DESIGN_FLAG_HIDE_FROM_WEB) >= 0) list.append("HIDE_FROM_WEB");
  if (flags.indexOf(DESIGN_FLAG_V4AGENT_DATA) >= 0) list.append("V4AGENT_DATA");
  if (flags.indexOf(DESIGN_FLAG_SUBFORM_NORENDER) >= 0) list.append("SUBFORM_NORENDER");
  if (flags.indexOf(DESIGN_FLAG_NO_MENU) >= 0) list.append("NO_MENU");
  if (flags.indexOf(DESIGN_FLAG_SACTIONS) >= 0) list.append("SACTIONS");
  if (flags.indexOf(DESIGN_FLAG_MULTILINGUAL_PRESERVE_HIDDEN) >= 0) list.append("MULTILINGUAL_PRESERVE_HIDDEN");
  if (flags.indexOf(DESIGN_FLAG_SERVLET) >= 0) list.append("SERVLET");
  if (flags.indexOf(DESIGN_FLAG_ACCESSVIEW) >= 0) list.append("ACCESSVIEW");
  if (flags.indexOf(DESIGN_FLAG_FRAMESET) >= 0) list.append("FRAMESET");
  if (flags.indexOf(DESIGN_FLAG_MULTILINGUAL_ELEMENT) >= 0) list.append("MULTILINGUAL_ELEMENT");
  if (flags.indexOf(DESIGN_FLAG_JAVA_RESOURCE) >= 0) list.append("JAVA_RESOURCE");
  if (flags.indexOf(DESIGN_FLAG_STYLESHEET_RESOURCE) >= 0) list.append("STYLESHEET_RESOURCE");
  if (flags.indexOf(DESIGN_FLAG_WEBSERVICE) >= 0) list.append("WEBSERVICE");
  if (flags.indexOf(DESIGN_FLAG_SHARED_COL) >= 0) list.append("SHARED_COL");
  if (flags.indexOf(DESIGN_FLAG_HIDE_FROM_MOBILE) >= 0) list.append("HIDE_FROM_MOBILE");
  if (flags.indexOf(DESIGN_FLAG_HIDE_FROM_PORTAL) >= 0) list.append("HIDE_FROM_PORTAL");
  if (flags.indexOf(DESIGN_FLAG_PROPFILE) >= 0) list.append("PROPFILE");
  if (flags.indexOf(DESIGN_FLAG_HIDE_FROM_V3) >= 0) list.append("HIDE_FROM_V3");
  if (flags.indexOf(DESIGN_FLAG_HIDE_FROM_V4) >= 0) list.append("HIDE_FROM_V4");
  if (flags.indexOf(DESIGN_FLAG_HIDE_FROM_V5) >= 0) list.append("HIDE_FROM_V5");
  if (flags.indexOf(DESIGN_FLAG_HIDE_FROM_V6) >= 0) list.append("HIDE_FROM_V6");
  if (flags.indexOf(DESIGN_FLAG_HIDE_FROM_V7) >= 0) list.append("HIDE_FROM_V7");
  if (flags.indexOf(DESIGN_FLAG_HIDE_FROM_V8) >= 0) list.append("HIDE_FROM_V8");
  if (flags.indexOf(DESIGN_FLAG_HIDE_FROM_V9) >= 0) list.append("HIDE_FROM_V9");
  if (flags.indexOf(DESIGN_FLAG_MUTILINGUAL_HIDE) >= 0) list.append("MUTILINGUAL_HIDE");
  if (flags.indexOf(DESIGN_FLAG_WEBHYBRIDDB) >= 0) list.append("WEBHYBRIDDB");
  if (flags.indexOf(DESIGN_FLAG_READONLY) >= 0) list.append("READONLY");
  if (flags.indexOf(DESIGN_FLAG_NEEDSREFRESH) >= 0) list.append("NEEDSREFRESH");
  if (flags.indexOf(DESIGN_FLAG_HTMLFILE) >= 0) list.append("HTMLFILE");
  if (flags.indexOf(DESIGN_FLAG_JSP) >= 0) list.append("JSP");
  if (flags.indexOf(DESIGN_FLAG_QUERYVIEW) >= 0) list.append("QUERYVIEW");
  if (flags.indexOf(DESIGN_FLAG_DIRECTORY) >= 0) list.append("DIRECTORY");
  if (flags.indexOf(DESIGN_FLAG_PRINTFORM) >= 0) list.append("PRINTFORM");
  if (flags.indexOf(DESIGN_FLAG_HIDEFROMDESIGNLIST) >= 0) list.append("HIDEFROMDESIGNLIST");
  if (flags.indexOf(DESIGN_FLAG_HIDEONLYFROMDESIGNLIST) >= 0) list.append("HIDEONLYFROMDESIGNLIST");
  if (flags.indexOf(DESIGN_FLAG_COMPOSITE_APP) >= 0) list.append("COMPOSITE_APP");
  if (flags.indexOf(DESIGN_FLAG_COMPOSITE_DEF) >= 0) list.append("COMPOSITE_DEF");
  if (flags.indexOf(DESIGN_FLAG_XSP_CC) >= 0) list.append("XSP_CC");
  if (flags.indexOf(DESIGN_FLAG_JS_SERVER) >= 0) list.append("JS_SERVER");
  if (flags.indexOf(DESIGN_FLAG_STYLEKIT) >= 0) list.append("STYLEKIT");
  if (flags.indexOf(DESIGN_FLAG_WIDGET) >= 0) list.append("WIDGET");
  if (flags.indexOf(DESIGN_FLAG_JAVAFILE) >= 0) list.append("JAVAFILE");
  this->propertyMap_.insert("Desgin flags", list.join("/"));
}
