﻿#ifndef DELETIONSTUBSITEM_H
#define DELETIONSTUBSITEM_H

#include "fileitem.h"

class DeletionStubsItem
    : public FileItem
{
public:
  DeletionStubsItem(
      const QString& text = QObject::tr("Deletion Stubs")
      , const QIcon& icon = QIcon(":/icons/trash.png")
      );
};

#endif // DELETIONSTUBSITEM_H
