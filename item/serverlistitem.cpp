﻿#include "item/serverlistitem.h"
#include "item/serveritem.h"
#include <ntlx/distinguishedname.h>

#include <QtDebug>

ServerListItem::ServerListItem()
  : TreeItem(QObject::tr("Servers"), QIcon(":/icons/earth-globe.png"))
{
}

void ServerListItem::refreshChildren()
{
  QList<QString> list;
  ntlx::Status status = ntlx::getServers(list);
  if (status.failure())
    return;

  for (auto it = list.begin(); it != list.end(); ++it)
  {
    ntlx::DistinguishedName name = *it;
    ServerItem* server = new ServerItem(name);
    appendRow(server);
  }
}
