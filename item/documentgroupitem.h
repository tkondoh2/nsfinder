﻿#ifndef DOCUMENTGROUPITEM_H
#define DOCUMENTGROUPITEM_H

#include "fileitem.h"

class NoteFileItem;

class DocumentGroupItem
    : public FileItem
{
public:
  /**
   * @brief コンストラクタ
   * @param text 表示テキスト
   * @param icon 表示アイコン
   */
  DocumentGroupItem(
      const QString& text = QObject::tr("Documents")
      , const QIcon& icon = QIcon(":/icons/text-file.png")
      );

  /**
   * @brief 子アイテムを更新する
   */
  virtual void refreshChildren();
};

#endif // DOCUMENTGROUPITEM_H
