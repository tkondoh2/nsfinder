﻿#include "item/notefileitem.h"
#include "item/dbinfoitem.h"
#include "item/aclgroupitem.h"
#include "item/designelementsitem.h"
#include "item/documentgroupitem.h"
#include "item/profilegroupitem.h"
#include "item/deletionstubsitem.h"
#include "treemodel.h"

#include <ntlx/database.h>
#include <ntlx/dbinfo.h>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <stdnames.h>

#if defined(NT)
#pragma pack(pop)
#endif

NoteFileItem::NoteFileItem(const QString& text, const QIcon& icon)
  : DirItem(text, icon)
{
}

void NoteFileItem::refreshChildren()
{
  ntlx::Database db(ntlx::PathSet(path_, server_, port_));

  auto dbInfoItem = new DbInfoItem;
  dbInfoItem->assign(*this);
  appendRow(dbInfoItem);

  auto aclGroupItem =  new ACLGroupItem;
  aclGroupItem->assign(*this);
  appendRow(aclGroupItem);

  {
    auto designsItem =  new DesignElementsItem(
          NOTE_CLASS_FORM
          , QObject::tr("Form Elements")
          );
    designsItem->assign(*this);
    appendRow(designsItem);
  }

  {
    auto designsItem =  new DesignElementsItem(
          NOTE_CLASS_VIEW
          , QObject::tr("View Elements")
          );
    designsItem->assign(*this);
    appendRow(designsItem);
  }

  {
    auto designsItem =  new DesignElementsItem(
          NOTE_CLASS_ICON
          , QObject::tr("Icon Elements")
          );
    designsItem->assign(*this);
    appendRow(designsItem);
  }

  {
    auto designsItem =  new DesignElementsItem(
          NOTE_CLASS_HELP_INDEX
          , QObject::tr("Help index Elements")
          );
    designsItem->assign(*this);
    appendRow(designsItem);
  }

  {
    auto designsItem =  new DesignElementsItem(
          NOTE_CLASS_HELP
          , QObject::tr("Help Elements")
          );
    designsItem->assign(*this);
    appendRow(designsItem);
  }

  {
    auto designsItem =  new DesignElementsItem(
          NOTE_CLASS_FILTER
          , QObject::tr("Filter Elements")
          );
    designsItem->assign(*this);
    appendRow(designsItem);
  }

  {
    auto designsItem =  new DesignElementsItem(
          NOTE_CLASS_FIELD
          , QObject::tr("Field Elements")
          );
    designsItem->assign(*this);
    appendRow(designsItem);
  }

  auto documentGroupItem =  new DocumentGroupItem;
  documentGroupItem->assign(*this);
  appendRow(documentGroupItem);

  auto profileGroupItem =  new ProfileGroupItem;
  profileGroupItem->assign(*this);
  appendRow(profileGroupItem);

  auto deletionStubsItem =  new DeletionStubsItem;
  deletionStubsItem->assign(*this);
  appendRow(deletionStubsItem);

  documentGroupItem->refreshChildren();
}

QVariant NoteFileItem::displayData(const TreeModel &model)
{
  switch (model.dbTextType())
  {
  case DbTextType::Filename:
    return getFilename();

  case DbTextType::Title:
    return getTitle();

  case DbTextType::TitleTemplate:
    return getTitleTemplate();
  }

  return QString();
}

QString NoteFileItem::getFilename() const
{
  if (propertyMap_.contains(DBDIR_PATH_ITEM))
    return propertyMap_[DBDIR_PATH_ITEM].toString();
  return data(Qt::DisplayRole).toString();
}

QString NoteFileItem::getTitle() const
{
  if (propertyMap_.contains(DBDIR_INFO_ITEM))
  {
    QString infoText(propertyMap_[DBDIR_INFO_ITEM].toString());
    ntlx::DbInfo dbInfo(ntlx::Lmbcs::fromQString(infoText));
    return dbInfo.getTitle();
  }
  return data(Qt::DisplayRole).toString();
}

QString NoteFileItem::getTitleTemplate() const
{
  if (propertyMap_.contains(DBDIR_INFO_ITEM))
  {
    QString infoText(propertyMap_[DBDIR_INFO_ITEM].toString());
    ntlx::DbInfo dbInfo(ntlx::Lmbcs::fromQString(infoText));
    QString title = dbInfo.getTitle();
    QString designClass = dbInfo.getDesignClass();
    if (designClass.isEmpty())
      return title;
    return QString("%1/%2").arg(title).arg(designClass);
  }
  return data(Qt::DisplayRole).toString();
}
