﻿#include "item/noteitem.h"
#include "treemodel.h"
#include "item/fileitem.h"
#include "item/itemitem.h"
#include <ntlx/database.h>
#include <ntlx/note.h>
#include <ntlx/item.h>

#include <QtDebug>

NoteItem::NoteItem(GLOBALINSTANCEID giid, const QIcon& icon)
  : TreeItem(QString(), icon)
{
  setGlobalInstanceId(giid);
}

UNIVERSALNOTEID NoteItem::unid() const
{
  UNIVERSALNOTEID uid;
  uid.File = oid_.File;
  uid.Note = oid_.Note;
  return uid;
}

void NoteItem::setGlobalInstanceId(GLOBALINSTANCEID giid)
{
  giid_ = giid;
}

void NoteItem::setOriginatorId(ORIGINATORID oid)
{
  oid_ = oid;
}

void NoteItem::setNoteId(NOTEID noteId)
{
  giid_.NoteID = noteId;
}

void NoteItem::setUniversalNoteId(UNIVERSALNOTEID unid)
{
  oid_.File = unid.File;
  oid_.Note = unid.Note;
}

void NoteItem::setNoteClass(WORD noteClass)
{
  noteClass_ = noteClass;
}

QVariant NoteItem::displayData(const TreeModel &model)
{
  QList<QString> titleList;
  if (model.noteTextType() & NoteTextType_NoteId)
  {
    titleList << getNoteId();
  }
  if (model.noteTextType() & NoteTextType_Title)
  {
    titleList << getTitle();
  }
  if (titleList.isEmpty())
    return QString();
  return titleList.join(" : ");
}

QString NoteItem::getNoteId() const
{
  return QString("%1").arg(giid_.NoteID, 0, 16);
}

QString NoteItem::getTitle() const
{
  return QString();
}

ntlx::PathSet NoteItem::pathSet() const
{
  auto parentItem = reinterpret_cast<FileItem*>(parent());
  return ntlx::PathSet( parentItem->path()
                      , parentItem->server()
                      , parentItem->port()
                      );
}

STATUS LNPUBLIC scanCallback(
    WORD /*Spare*/
    , WORD /*ItemFlags*/
    , char* Name
    , WORD NameLength
    , void* /*Value*/
    , DWORD /*ValueLength*/
    , void* /*RoutinParameter*/
    )
{
  ntlx::Lmbcs lName(Name, NameLength);
  qDebug() << lName.toQString();
  return NOERROR;
}

void NoteItem::refreshChildren()
{
  ntlx::Database db(pathSet());
  ntlx::Note note(db, noteId());
  note.open();

  NSFItemScan(note, scanCallback, nullptr);

  QList<ntlx::Item0*> items;
  if (note.getItems(items).lastStatus().failure())
    return;
  for (auto it = items.begin(); it != items.end(); ++it)
  {
    ntlx::Item0* item = *it;
//    qDebug() << item.name() << item.typeName() << item.flagList();
    ItemItem* itemItem = new ItemItem(item->name());
    itemItem->setFlags(item->flags());
    itemItem->setValue(item->type(), item->value());
    QMap<QString,QVariant> props;
    item->getProperties(props);
    itemItem->setProperties(props);
    appendRow(itemItem);
  }
  qDeleteAll(items);
}
