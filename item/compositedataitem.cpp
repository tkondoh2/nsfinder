﻿#include "compositedataitem.h"

#include <ntlx/richtextitem.h>
#include <ntlx/lmbcs.h>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <editods.h>

#if defined(NT)
#pragma pack(pop)
#endif

CompositeDataItem::CompositeDataItem(const QString& text, const QIcon& icon)
  : TreeItem(text, icon)
{
}

void CompositeDataItem::setValue(WORD type, char* value)
{
  char* pValue = value;
  switch (type)
  {
  case SIG_CD_TEXT:
  {
    CDTEXT cd;
    ODSReadMemory(&pValue, _CDTEXT, &cd, 1);
    WORD len = cd.Header.Length - ODSLength(_CDTEXT);
    ntlx::Lmbcs text(pValue, len);
    QString qText = ntlx::nullToCR(text.toQString());
    propertyMap_[QObject::tr("Text")] = qText;
    setData(QString("TEXT [%1]")
            .arg(qText)
            , Qt::DisplayRole
            );
  }
    break;
  }
}
