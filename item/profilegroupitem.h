﻿#ifndef PROFILEGROUPITEM_H
#define PROFILEGROUPITEM_H

#include "fileitem.h"

class ProfileGroupItem
    : public FileItem
{
public:
  ProfileGroupItem(
      const QString& text = QObject::tr("Profiles")
      , const QIcon& icon = QIcon(":/icons/text-file-1.png")
      );
};

#endif // PROFILEGROUPITEM_H
