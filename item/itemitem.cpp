﻿#include "itemitem.h"
#include "noteitem.h"
#include "item/cd/cdtextitem.h"
#include "item/cd/cdfonttable.h"
#include "item/cd/cdotheritem.h"
#include <ntlx/database.h>
#include <ntlx/note.h>
#include <ntlx/item.h>
#include <ntlx/richtextitem.h>

#include <QtDebug>

ItemItem::ItemItem(
    const QString& name
    , const QIcon& icon
    )
  : TreeItem(name, icon)
{
}

void ItemItem::setFlags(WORD flags)
{
  QStringList list = ntlx::Item0::flagList(flags);
  propertyMap_.insert(QObject::tr("Flags"), list.join(","));
}

void ItemItem::setValue(WORD type, const QByteArray &value)
{
  propertyMap_.insert(
        QObject::tr("Type/Class")
        , ntlx::Item0::typeName(type)
        );

  int maxSize = value.size() > 16 * 1024 ? 16 * 1024 : value.size();
  QVariant v = ntlx::Item0::toVariant(type, value.constData(), maxSize);
  QString text = ntlx::Item0::variantToString(v);
  propertyMap_.insert(QObject::tr("Value"), text);
  propertyMap_[QObject::tr("Size")] = QString::number(value.size());
}

void ItemItem::setProperties(QMap<QString,QVariant>& props)
{
  for (auto it = props.constBegin(); it != props.constEnd(); ++it)
  {
    propertyMap_[it.key()] = it.value().toString();
  }
}

template <class T>
STATUS addCdItem(ItemItem* itemItem, char** ppRecord)
{
  T* cdItem = new T(ppRecord);
  itemItem->appendRow(cdItem);
  return NOERROR;
}

STATUS LNPUBLIC refreshComposite(
    char* recordPtr
    , WORD recordType
    , DWORD /*recordLength*/
    , void* contextPtr
    )
{
  auto itemItem = reinterpret_cast<ItemItem*>(contextPtr);
  QString cdName = ntlx::RichTextItem::cdTypeName(recordType);
  qDebug() << cdName;

  switch (recordType)
  {
  case SIG_CD_TEXT:
    return addCdItem<CdTextItem>(itemItem, &recordPtr);

  case SIG_CD_FONTTABLE:
    return addCdItem<CdFontTable>(itemItem, &recordPtr);
  }

  return addCdItem<CdOtherItem>(itemItem, &recordPtr);

//  CompositeDataItem* cdItem = new CompositeDataItem(cdName);
//  cdItem->setValue(recordType, recordPtr);
//  itemItem->appendRow(cdItem);
//  return NOERROR;
}

void ItemItem::refreshChildren()
{
  auto parentItem = reinterpret_cast<NoteItem*>(parent());

  ntlx::Database db(parentItem->pathSet());
  if (db.lastStatus().failure()) return;

  ntlx::Note note(db, parentItem->noteId());
  if (db.lastStatus().failure()) return;

  ntlx::RichTextItem::enumerate(note, text(), refreshComposite, this);
}
