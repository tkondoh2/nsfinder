﻿#ifndef DESIGNNOTEITEM_H
#define DESIGNNOTEITEM_H

#include "item/noteitem.h"
#include <ntlx/note.h>

class DesignNoteItem
    : public NoteItem
{
public:
  DesignNoteItem(
      GLOBALINSTANCEID giid
      );

  /**
   * @brief タイトルを取得する
   * @return
   */
  virtual QString getTitle() const;

  void getDesignTitle(ntlx::Note& note);

  void getDesignFlags(ntlx::Note& note);

protected:
  QString title_;
};

#endif // DESIGNNOTEITEM_H
