﻿#include "designelementsitem.h"
#include "item/designnoteitem.h"
#include <ntlx/database.h>

DesignElementsItem::DesignElementsItem(
    WORD noteClass
    , const QString& text
    , const QIcon& icon
    )
  : FileItem(text, icon)
  , noteClass_(noteClass)
  , titleText_(text)
{
}

void DesignElementsItem::refreshChildren()
{
  ntlx::Database db(ntlx::PathSet(path_, server_, port_));

  ntlx::SummaryList list;
  if (db.collectNotes(list, noteClass_).failure())
    return;

  for (auto it = list.begin(); it != list.end(); ++it)
  {
    auto noteItem = refreshNote<DesignNoteItem>(*it);
    ntlx::Note note(db, noteItem->noteId());
    noteItem->getDesignTitle(note);
    noteItem->getDesignFlags(note);
  }

  setData(QObject::tr("%1 (%2)")
          .arg(titleText_)
          .arg(rowCount()), Qt::DisplayRole)
      ;
}
