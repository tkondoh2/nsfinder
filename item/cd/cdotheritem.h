﻿#ifndef CDOTHERITEM_H
#define CDOTHERITEM_H

#include "item/cditem.h"
#include <ntlx/cd/other.h>

class CdOtherItem
    : public CdItem<ntlx::cd::Other>
{
public:
  CdOtherItem(char** ppRecord);
};

#endif // CDOTHERITEM_H
