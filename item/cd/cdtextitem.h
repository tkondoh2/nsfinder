﻿#ifndef CDTEXTITEM_H
#define CDTEXTITEM_H

#include "item/cditem.h"
#include <ntlx/cd/text.h>

class CdTextItem
    : public CdItem<ntlx::cd::Text>
{
public:
  CdTextItem(char** ppRecord);
};

#endif // CDTEXTITEM_H
