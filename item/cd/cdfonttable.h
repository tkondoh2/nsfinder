﻿#ifndef CDFONTTABLE_H
#define CDFONTTABLE_H

#include "item/cditem.h"
#include <ntlx/cd/fonttable.h>

class CdFontTable
    : public CdItem<ntlx::cd::FontTable>
{
public:
  CdFontTable(char** ppRecord);
};

#endif // CDFONTTABLE_H
