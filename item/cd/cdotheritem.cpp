﻿#include "cdotheritem.h"
#include <ntlx/richtextitem.h>

CdOtherItem::CdOtherItem(char** ppRecord)
  : CdItem(ppRecord)
{
  QString typeName = ntlx::RichTextItem::cdTypeName(cd_.getSignature());
  setData(QObject::tr("CD(%1)").arg(typeName), Qt::DisplayRole);
  propertyMap_.insert(QObject::tr("Type name"), typeName);
  propertyMap_.insert(QObject::tr("Record data"), cd_.toString());
}
