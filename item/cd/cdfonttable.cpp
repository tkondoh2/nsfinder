﻿#include "cdfonttable.h"

CdFontTable::CdFontTable(char** ppRecord)
  : CdItem(ppRecord)
{
  setData(QObject::tr("Font Table"), Qt::DisplayRole);
  propertyMap_[QObject::tr("Value")] = cd_.toString();
}
