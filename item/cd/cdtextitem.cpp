﻿#include "item/cd/cdtextitem.h"

#define BOOLTEXT(x) (x ? "True" : "False")
#define BYTETEXT(x) (QString("%1").arg(static_cast<uint>(x)))

CdTextItem::CdTextItem(char** ppRecord)
  : CdItem(ppRecord)
{
  setData(QObject::tr("Text"), Qt::DisplayRole);
  propertyMap_[QObject::tr("Value")] = cd_.toString();
  propertyMap_[QObject::tr("Size")] = BYTETEXT(cd_.fontId().size());
  propertyMap_[QObject::tr("Color")] = BYTETEXT(cd_.fontId().color());
  propertyMap_[QObject::tr("Face")] = BYTETEXT(cd_.fontId().face());
  propertyMap_[QObject::tr("Bold")] = BOOLTEXT(cd_.fontId().isBold());
  propertyMap_[QObject::tr("Italic")] = BOOLTEXT(cd_.fontId().isItalice());
  propertyMap_[QObject::tr("Underline")] = BOOLTEXT(cd_.fontId().isUnderline());
  propertyMap_[QObject::tr("StrikeOut")] = BOOLTEXT(cd_.fontId().isStrikeOut());
  propertyMap_[QObject::tr("Super")] = BOOLTEXT(cd_.fontId().isSuper());
  propertyMap_[QObject::tr("Sub")] = BOOLTEXT(cd_.fontId().isSub());
  propertyMap_[QObject::tr("Effect")] = BOOLTEXT(cd_.fontId().isEffect());
  propertyMap_[QObject::tr("Shadow")] = BOOLTEXT(cd_.fontId().isShadow());
  propertyMap_[QObject::tr("Emboss")] = BOOLTEXT(cd_.fontId().isEmboss());
  propertyMap_[QObject::tr("Extrude")] = BOOLTEXT(cd_.fontId().isExtrude());
}
