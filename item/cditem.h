﻿#ifndef CDITEM_H
#define CDITEM_H

#include "treeitem.h"
#include <ntlx/status.h>

template <class T>
class CdItem
    : public TreeItem
{
public:
  CdItem(char** ppRecord)
    : TreeItem("", QIcon())
    , cd_(ppRecord)
  {
  }

protected:
  T cd_;
};

#endif // CDITEM_H
