﻿#ifndef COMPOSITEDATAITEM_H
#define COMPOSITEDATAITEM_H

#include "treeitem.h"
#include <ntlx/status.h>

class CompositeDataItem
    : public TreeItem
{
public:
  CompositeDataItem(const QString& text, const QIcon& icon = QIcon());

  void setValue(WORD type, char* value);
};

#endif // COMPOSITEDATAITEM_H
