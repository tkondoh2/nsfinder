﻿#ifndef SERVERITEM_H
#define SERVERITEM_H

#include "item/diritem.h"
#include <ntlx/distinguishedname.h>

/**
 * @brief サーバーアイテム
 */
class ServerItem
    : public DirItem
{
public:
  /**
   * @brief コンストラクタ
   * @param serverName サーバー名
   */
  ServerItem(const ntlx::DistinguishedName& serverName);
};

#endif // SERVERITEM_H
