﻿#ifndef DOCUMENTITEM_H
#define DOCUMENTITEM_H

#include "item/noteitem.h"
#include <ntlx/database.h>

/**
 * @brief 文書アイテム
 */
class DocumentItem
    : public NoteItem
{
public:
  /**
   * @brief コンストラクタ
   * @param giid グローバルインスタンスID
   * @param icon 表示アイコン
   */
  DocumentItem(
      GLOBALINSTANCEID giid
      , const QIcon& icon = QIcon(":/icons/text-file.png")
      );

  /**
   * @brief タイトルを取得する
   * @return
   */
  virtual QString getTitle() const;

  /**
   * @brief ウィンドウタイトルを計算する
   * @param db データベースへの参照
   */
  void computeWindowTitle(ntlx::Database& db);

private:
  QString windowTitle_;
};

#endif // DOCUMENTITEM_H
