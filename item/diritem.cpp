﻿#include "item/diritem.h"

#include <ntlx/pathset.h>
#include <ntlx/netpath.h>
#include <ntlx/dbinfo.h>
#include "item/notefileitem.h"

#include <QtDebug>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <stdnames.h>

#if defined(NT)
#pragma pack(pop)
#endif

DirItem::DirItem(const QString& text, const QIcon& icon)
  : FileItem(text, icon)
{
}

void DirItem::refreshChildren()
{
  // 自身のパスの直下にあるDBファイルとディレクトリを取得する
  ntlx::PathSet pathSet(path_, server_, port_);
  ntlx::SummaryList files;
  if (pathSet.toNetPath()
      .collectFiles(files, FILE_ANYNOTEFILE | FILE_DIRS | FILE_NOUPDIRS)
      .failure())
    return;

  // 取得できたデータをディレクトリ、ノートファイル(DB)に分けて展開する
  for (auto it = files.begin(); it != files.end(); ++it)
  {
    QString type = (*it).map().take(DBDIR_TYPE_ITEM).toString();
    if (type == DBDIR_TYPE_ITEM_DIRECTORY)
      refreshMap<DirItem>(*it, (*it).map().value(FIELD_TITLE).toString());
    else
    {
      QString path = (*it).map().value(DBDIR_PATH_ITEM).toString();
      QString iconPath = (path.lastIndexOf(QRegExp("\\.nt.?$")) < 0)
          ? ":/icons/book.png"
          : ":/icons/puzzle-piece-silhouette.png";
      QString infoText((*it).map().value(DBDIR_INFO_ITEM).toString());
      ntlx::DbInfo dbInfo(ntlx::Lmbcs::fromQString(infoText));

      refreshMap<NoteFileItem>(*it, dbInfo.getTitle(), iconPath);
    }
  }
}

template <class T>
void DirItem::refreshMap(
    ntlx::SummaryMap &map
    , const QString& text
    , const QString& iconPath
    )
{
  T* item = new T(text, QIcon(iconPath));
  item->server_ = server_;
  item->port_ = port_;
  item->path_ = map.map().value(DBDIR_PATH_ITEM).toString();
  for (auto prop = map.map().begin(); prop != map.map().end(); ++prop)
    item->propertyMap_.insert(prop.key(), prop.value());
  appendRow(item);
}
