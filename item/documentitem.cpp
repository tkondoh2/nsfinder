﻿#include "item/documentitem.h"
#include <ntlx/note.h>

DocumentItem::DocumentItem(GLOBALINSTANCEID giid, const QIcon& icon)
  : NoteItem(giid, icon)
  , windowTitle_()
{
}

QString DocumentItem::getTitle() const
{
  return windowTitle_;
}

void DocumentItem::computeWindowTitle(ntlx::Database &db)
{
  ntlx::Note note(db, noteId());
  windowTitle_ = note.getWindowTitle();
  if (note.lastStatus().failure())
    windowTitle_ = note.lastStatus().toMessage();
}
