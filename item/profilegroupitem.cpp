﻿#include "profilegroupitem.h"

ProfileGroupItem::ProfileGroupItem(
    const QString& text
    , const QIcon& icon
    )
  : FileItem(text, icon)
{
}
