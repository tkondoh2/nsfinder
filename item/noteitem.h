﻿#ifndef NOTEITEM_H
#define NOTEITEM_H

#include "treeitem.h"
#include <ntlx/status.h>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <nsfsearc.h>

#if defined(NT)
#pragma pack(pop)
#endif

namespace ntlx {
class PathSet;
}

/**
 * @brief ノートアイテム
 */
class NoteItem
    : public TreeItem
{
public:
  /**
   * @brief コンストラクタ
   * @param giid グローバルインスタンスID
   * @param icon アイコン
   */
  NoteItem(GLOBALINSTANCEID giid, const QIcon& icon);

  /**
   * @brief グローバルインスタンスIDを返す
   * @return グローバルインスタンスID
   */
  GLOBALINSTANCEID globalInstanceId() const { return giid_; }

  /**
   * @brief オリジネーターIDを返す
   * @return オリジネーターID
   */
  ORIGINATORID originatorId() const { return oid_; }

  /**
   * @brief ノートIDを返す
   * @return ノートID
   */
  NOTEID noteId() const { return giid_.NoteID; }

  /**
   * @brief ユニバーサルノートIDを返す
   * @return ユニバーサルノートID
   */
  UNIVERSALNOTEID unid() const;

  /**
   * @brief ノートクラスを返す
   * @return ノートクラス
   */
  WORD noteClass() const { return noteClass_; }

  /**
   * @brief グローバルインスタンスIDを設定する
   * @param giid グローバルインスタンスID
   */
  void setGlobalInstanceId(GLOBALINSTANCEID giid);

  /**
   * @brief オリジネーターIDを設定する
   * @param oid オリジネーターID
   */
  void setOriginatorId(ORIGINATORID oid);

  /**
   * @brief ノートIDを設定する
   * @param noteId ノートID
   */
  void setNoteId(NOTEID noteId);

  /**
   * @brief ユニバーサルノートIDを設定する
   * @param unid ユニバーサルノートID
   */
  void setUniversalNoteId(UNIVERSALNOTEID unid);

  /**
   * @brief ノートクラスを設定する
   * @param noteClass ノートクラス
   */
  void setNoteClass(WORD noteClass);

  /**
   * @brief 表示用データを取得する
   * @param model ツリーモデル
   * @return 表示用データ
   */
  virtual QVariant displayData(const TreeModel& model);

  /**
   * @brief ノートID文字列を取得する
   * @return ノートID文字列
   */
  virtual QString getNoteId() const;

  /**
   * @brief タイトルを取得する
   * @return タイトル
   */
  virtual QString getTitle() const;

  ntlx::PathSet pathSet() const;

  /**
   * @brief 子アイテムを更新する
   */
  virtual void refreshChildren();

private:
  GLOBALINSTANCEID giid_;
  ORIGINATORID oid_;
  WORD noteClass_;
};

#endif // NOTEITEM_H
