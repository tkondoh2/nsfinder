﻿#ifndef FILEITEM_H
#define FILEITEM_H

#include "treeitem.h"

#include <ntlx/summarymap.h>

class FileItem
    : public TreeItem
{
public:
  FileItem(
      const QString& text
      , const QIcon& icon
      );

  void assign(const FileItem& other);

  QString path() const { return path_; }

  QString server() const { return server_; }

  QString port() const { return port_; }

//  void setPath(const QString& path) { path_ = path; }

//  void setServer(const QString& server) { server_ = server; }

//  void setPort(const QString& port) { port_ = port; }

protected:

  template <class T>
  T* refreshNote(ntlx::SummaryMap& map)
  {
    T* item = new T(map.match().ID);
    item->setGlobalInstanceId(map.match().ID);
    item->setOriginatorId(map.match().OriginatorID);
    item->setNoteClass(map.match().NoteClass);
    for (auto prop = map.map().begin(); prop != map.map().end(); ++prop)
      item->insertProperty(prop.key(), prop.value());
    appendRow(item);
    return item;
  }

  QString path_;
  QString server_;
  QString port_;
};

#endif // FILEITEM_H
