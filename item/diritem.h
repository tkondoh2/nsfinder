﻿#ifndef DIRITEM_H
#define DIRITEM_H

#include "fileitem.h"

namespace ntlx {
class SummaryMap;
}

/**
 * @brief ディレクトリアイテム
 */
class DirItem
    : public FileItem
{
public:
  /**
   * @brief コンストラクタ
   * @param text 表示テキスト
   * @param icon 表示アイコン
   */
  DirItem(
      const QString& text
      , const QIcon& icon = QIcon(":/icons/folder-white-shape.png")
      );

  /**
   * @brief 子アイテムを更新する
   */
  virtual void refreshChildren();

protected:

  template <class T>
  void refreshMap(
      ntlx::SummaryMap &map
      , const QString& text
      , const QString& iconPath = ":/icons/folder-white-shape.png"
      );
};

#endif // DIRITEM_H
