﻿#include "item/serveritem.h"

ServerItem::ServerItem(const ntlx::DistinguishedName& serverName)
  : DirItem(serverName.abbreviated(), QIcon(":/icons/sitemap.png"))
{
  server_ = serverName.canonical();
}
