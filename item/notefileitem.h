﻿#ifndef NOTEFILEITEM_H
#define NOTEFILEITEM_H

#include "item/diritem.h"

namespace ntlx {
class SummaryMap;
}

/**
 * @brief ノートファイル(データベース)アイテム
 */
class NoteFileItem
    : public DirItem
{
public:
  /**
   * @brief コンストラクタ
   * @param text 表示テキスト
   * @param icon 表示アイコン
   */
  NoteFileItem(
      const QString& text
      , const QIcon& icon = QIcon(":/icons/book.png")
      );

  /**
   * @brief 子アイテムを更新する
   */
  virtual void refreshChildren();

  /**
   * @brief 表示用データを取得する
   * @param model ツリーモデル
   * @return 表示用データ
   */
  virtual QVariant displayData(const TreeModel &model);

protected:

  template <class T>
  T* refreshNote(ntlx::SummaryMap& map);

  /**
   * @brief ファイル名を取得する
   * @return ファイル名
   */
  QString getFilename() const;

  /**
   * @brief タイトルを取得する
   * @return
   */
  QString getTitle() const;

  /**
   * @brief タイトル＋テンプレート名を取得する
   * @return
   */
  QString getTitleTemplate() const;
};

#endif // NOTEFILEITEM_H
