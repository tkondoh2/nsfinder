﻿#ifndef LOCALDATADIRITEM_H
#define LOCALDATADIRITEM_H

#include "item/diritem.h"

/**
 * @brief ローカルデータディレクトリアイテム
 */
class LocalDataDirItem
    : public DirItem
{
public:
  /**
   * @brief コンストラクタ
   */
  LocalDataDirItem();
};

#endif // LOCALDATADIRITEM_H
