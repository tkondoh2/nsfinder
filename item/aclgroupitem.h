﻿#ifndef ACLGROUPITEM_H
#define ACLGROUPITEM_H

#include "fileitem.h"

class ACLGroupItem
    : public FileItem
{
public:
  ACLGroupItem(
      const QString& text = QObject::tr("Access Control List")
      , const QIcon& icon = QIcon(":/icons/padlock.png")
      );
};

#endif // ACLGROUPITEM_H
