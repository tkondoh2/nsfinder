﻿#ifndef SERVERLISTITEM_H
#define SERVERLISTITEM_H

#include "treeitem.h"

/**
 * @brief サーバーリストアイテム
 */
class ServerListItem
    : public TreeItem
{
public:
  /**
   * @brief コンストラクタ
   */
  ServerListItem();

  /**
   * @brief 子アイテムを更新する
   */
  virtual void refreshChildren();
};

#endif // SERVERLISTITEM_H
