﻿#ifndef DBINFOITEM_H
#define DBINFOITEM_H

#include "fileitem.h"

class DbInfoItem
    : public FileItem
{
public:
  DbInfoItem(
      const QString& text = QObject::tr("Database Information")
      , const QIcon& icon = QIcon(":/icons/information-button.png")
      );
};

#endif // DBINFOITEM_H
