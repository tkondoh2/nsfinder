﻿#ifndef ITEMITEM_H
#define ITEMITEM_H

#include "treeitem.h"
#include <ntlx/status.h>

class ItemItem
    : public TreeItem
{
public:
  ItemItem(
      const QString& name = ""
      , const QIcon& icon = QIcon(":/icons/three-small-square-shapes.png")
      );

  virtual void setFlags(WORD flags);
  virtual void setValue(WORD type, const QByteArray& value);
  virtual void setProperties(QMap<QString,QVariant>& props);

  /**
   * @brief 子アイテムを更新する
   */
  virtual void refreshChildren();
};

#endif // ITEMITEM_H
