﻿#ifndef TREEITEM_H
#define TREEITEM_H

#include <QStandardItem>
#include "definitions.h"

class TreeModel;

/**
 * @brief ツリーアイテムの基本クラス
 */
class TreeItem
    : public QStandardItem
{
public:
  /**
   * @brief コンストラクタ
   * @param text 表示テキスト
   * @param icon 表示アイコン
   */
  TreeItem(const QString& text, const QIcon& icon);

  /**
   * @brief プロパティマップを返す
   * @return プロパティマップ
   */
  const PropertyMap& propertyMap() const { return propertyMap_; }

  /**
   * @brief プロパティを挿入する
   * @param key プロパティキー
   * @param value プロパティ値
   */
  void insertProperty(const QString& key, const QVariant& value);

  /**
   * @brief 子アイテムを更新する
   */
  virtual void refreshChildren(){}

  /**
   * @brief 表示用データを取得する
   * @param model ツリーモデル
   * @return 表示用データ
   */
  virtual QVariant displayData(const TreeModel& model);

protected:
  PropertyMap propertyMap_;
};

#endif // TREEITEM_H
