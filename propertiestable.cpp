﻿#include "propertiestable.h"
#include <QDateTime>
#include <ntlx/number.h>
#include <ntlx/timedate.h>

/**
 * @brief RANGE型を格納するクラス
 */
typedef QMap<QString, QVariant> RangeMap;

PropertiesTable::PropertiesTable(QWidget *parent)
  : QTableView(parent)
{
  model_.setHorizontalHeaderLabels(QStringList() << tr("Name") << tr("Value"));
  setModel(&model_);
}

/**
 * @brief RANGE型をフォーマットして出力するテンプレート関数
 * @note T ntlx::Numberまたはntlx::TimeDate
 */
template<class T, typename S, typename P>
QStandardItem* formatRangeMap(const RangeMap& map)
{
  QStringList dspList;

  // Listを処理する
  QList<QVariant> list = map["list"].toList();
  for (auto it = list.begin(); it != list.end(); ++it)
  {
    T value((*it).value<S>());
    dspList.append(value.toString());
  }

  // Rangeを処理する
  QList<QVariant> pairs = map["range"].toList();
  for (auto it = pairs.begin(); it != pairs.end(); ++it)
  {
    P pair = (*it).value<P>();
    T lower(pair.Lower);
    T upper(pair.Upper);
    dspList.append(QString("%1..%2")
                   .arg(lower.toString())
                   .arg(upper.toString())
                   );
  }
  return new QStandardItem(dspList.join(", "));
}

void PropertiesTable::write(const PropertyMap& map)
{
  // すべてのアイテムを削除する
  model_.removeRows(0, model_.rowCount());

  for (auto it = map.begin(); it != map.end(); ++it)
  {
    QStandardItem* pName = new QStandardItem(it.key());
    QStandardItem* pValue = nullptr;
    QVariant value = it.value();
    switch (value.type())
    {
    // テキスト
    case QMetaType::QString:
      pValue = new QStandardItem(value.toString());
      break;

    // テキストリスト
    case QMetaType::QStringList:
    {
      QStringList dspList = value.toStringList();
      pValue = new QStandardItem(dspList.join(", "));
    }
      break;

    // 数値
    case QMetaType::Double:
      pValue = new QStandardItem(QString::number(value.toDouble()));
      break;

    // 日時
    case QMetaType::QDateTime:
      pValue = new QStandardItem(
            value.toDateTime().toString("yyyy-MM-dd HH:mm:ss")
            );
      break;

    // 数値リストまたは日時リスト
    case QMetaType::QVariantMap:
    {
      RangeMap rangeMap = value.toMap();
      if (rangeMap["type"] == "number")
        pValue = formatRangeMap<ntlx::Number, NUMBER, NUMBER_PAIR>(rangeMap);
      else if (rangeMap["type"] == "timedate")
        pValue = formatRangeMap<ntlx::TimeDate, TIMEDATE, TIMEDATE_PAIR>(rangeMap);
    }
      break;

    // その他
    default:
      pValue = new QStandardItem;
      break;
    }

    QList<QStandardItem*> list;
    list << pName << pValue;
    model_.appendRow(list);
  }

  // 幅と高さをコンテンツに合わせてリサイズする
  resizeColumnsToContents();
  resizeRowsToContents();
}
