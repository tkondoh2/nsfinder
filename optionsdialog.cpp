﻿#include "optionsdialog.h"
#include "ui_optionsdialog.h"

#include <QtDebug>
#include <QSettings>

#include "definitions.h"

OptionsDialog::OptionsDialog(QWidget *parent)
  : QDialog(parent)
  , ui_(new Ui::OptionsDialog)
{
  ui_->setupUi(this);

  // 設定情報で初期化する
  loadSettings();

  // データベーステキストラジオボタンの接続
  connect(ui_->dbTextGroup, SIGNAL(buttonClicked(QAbstractButton*))
          , this, SLOT(selectDbText(QAbstractButton*))
          );

  // 文書ID、文書タイトルチェックボックスの接続
  connect(ui_->noteIdCheckBox, SIGNAL(toggled(bool))
          , this, SLOT(selectNoteText(bool))
          );
  connect(ui_->noteTitleCheckBox, SIGNAL(toggled(bool))
          , this, SLOT(selectNoteText(bool))
          );
}

OptionsDialog::~OptionsDialog()
{
  delete ui_;
}

void OptionsDialog::loadSettings()
{
  QSettings settings;
  DbTextType dbTextType = (DbTextType)settings.value(
        "DbTextType"
        , (int)DbTextType::Filename
        ).toInt()
      ;
  uint noteTextType = settings.value(
        "NoteTextType"
        , NoteTextType_NoteId
        ).toUInt()
      ;

  switch (dbTextType)
  {
  case DbTextType::TitleTemplate:
    ui_->dbTemplateRadio->setChecked(true);
    break;

  case DbTextType::Title:
    ui_->dbTitleRadio->setChecked(true);
    break;

  default:
    ui_->dbFilenameRadio->setChecked(true);
    break;
  }

  ui_->noteIdCheckBox->setChecked(noteTextType & NoteTextType_NoteId);
  ui_->noteTitleCheckBox->setChecked(noteTextType & NoteTextType_Title);
}

void OptionsDialog::selectDbText(QAbstractButton* button)
{
  DbTextType type = DbTextType::Filename;
  if (button == ui_->dbTitleRadio)
    type = DbTextType::Title;
  else if (button == ui_->dbTemplateRadio)
    type = DbTextType::TitleTemplate;

  QSettings settings;
  settings.setValue("DbTextType", (int)type);
}

void OptionsDialog::selectNoteText(bool)
{
  uint value = 0;
  if (ui_->noteIdCheckBox->isChecked())
    value |= NoteTextType_NoteId;
  if (ui_->noteTitleCheckBox->isChecked())
    value |= NoteTextType_Title;

  QSettings settings;
  settings.setValue("NoteTextType", value);
}
