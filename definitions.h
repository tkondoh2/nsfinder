﻿#ifndef DEFINITIONS_H
#define DEFINITIONS_H

#include <QMap>
#include <QVariant>
#include <QString>

/**
 * @brief プロパティマップ
 */
typedef QMap<QString, QVariant> PropertyMap;

/**
 * @brief データベーステキストタイプ
 */
enum class DbTextType
{
  Filename
  , Title
  , TitleTemplate
};

/**
 * @brief ノートテキストタイプ
 */
const uint NoteTextType_NoteId = 1;
const uint NoteTextType_Title = 2;

#endif // DEFINITIONS_H
