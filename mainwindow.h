﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "definitions.h"

namespace Ui {
class MainWindow;
}

class TreeModel;

/**
 * @brief メインウィンドウクラス
 */
class MainWindow
    : public QMainWindow
{
  Q_OBJECT

public:
  /**
   * @brief コンストラクタ
   * @param parent 親ウィジェットへのポインタ
   */
  explicit MainWindow(QWidget *parent = 0);

  /**
   * デストラクタ
   */
  ~MainWindow();

public slots:
  /**
   * @brief ツリービュー用のコンテキストメニューを開く
   * @param pos 位置情報
   */
  void treeContextMenu(const QPoint& pos);

  /**
   * @brief オプションダイアログを実行する
   */
  void execOptionsDialog();

private:
  Ui::MainWindow *ui_;
  TreeModel* model_;
};

#endif // MAINWINDOW_H
