﻿#ifndef TREEVIEW_H
#define TREEVIEW_H

#include <QTreeView>

class TreeModel;

/**
 * @brief ツリービュークラス
 */
class TreeView
    : public QTreeView
{
  Q_OBJECT
public:
  /**
   * @brief コンストラクタ
   * @param parent 親ウィジェットへのポインタ
   */
  explicit TreeView(QWidget* parent = 0);

  /**
   * @brief ツリーモデルへのポインタを返す
   * @return ツリーモデルへのポインタ
   */
  TreeModel* treeModel() { return (TreeModel*)model(); }

public slots:
  /**
   * @brief 子アイテムを更新する
   */
  void refreshChildren();
};

#endif // TREEVIEW_H
